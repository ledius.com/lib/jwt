/**
 *  Ledius LLC
 *  Copyright (C) 17 Jul 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import {
  TokenPayload,
  TokenType,
  TypeSeparatedInterface,
} from '@/jwt/interfaces/token-payload.interface';

@Injectable()
export abstract class AbstractGuard<
  T extends TypeSeparatedInterface = TokenPayload,
> implements CanActivate
{
  public constructor(protected readonly jwtService: JwtService) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const token = this.getAccessTokenFromContext(context);
    const tokenPayload = await this.jwtService.verifyAsync<T>(token);
    this.verityTokenTypes(tokenPayload);
    this.setPayloadToContext(context, { ...tokenPayload, token });
    return true;
  }

  protected abstract getSupportedTokenTypes(): TokenType[];

  private getAccessTokenFromContext = (context: ExecutionContext): string => {
    const request: Request = context.switchToHttp().getRequest();
    const authorization = request.headers.authorization;

    if (!authorization) {
      throw new UnauthorizedException();
    }
    const accessToken = authorization.split(' ')[1];
    if (!accessToken) {
      throw new UnauthorizedException();
    }
    return accessToken;
  };

  private setPayloadToContext = <T>(
    context: ExecutionContext,
    payload: T,
  ): void => {
    const request: Request & {
      tokenPayload: T;
    } = context.switchToHttp().getRequest();
    request.tokenPayload = payload;
  };

  private verityTokenTypes(payload: TypeSeparatedInterface): void {
    const supportedType = this.getSupportedTokenTypes().find(
      (type) => type === payload.type,
    );
    if (!supportedType) {
      throw new UnauthorizedException(`Invalid token type: ${payload.type}`);
    }
  }
}
