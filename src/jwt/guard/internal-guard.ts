import { CanActivate, Injectable } from '@nestjs/common';
import { AbstractGuard } from '@/jwt/guard/abstract-guard';
import {
  InternalPayload,
  TokenType,
} from '@/jwt/interfaces/token-payload.interface';

@Injectable()
export class InternalGuard
  extends AbstractGuard<InternalPayload>
  implements CanActivate
{
  protected getSupportedTokenTypes(): TokenType[] {
    return [TokenType.INTERNAL];
  }
}
