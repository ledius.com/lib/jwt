import { AbstractGuard } from '@/jwt/guard/abstract-guard';
import { CanActivate, Injectable } from '@nestjs/common';
import {
  TokenPayload,
  TokenType,
} from '@/jwt/interfaces/token-payload.interface';

@Injectable()
export class AuthGuard
  extends AbstractGuard<TokenPayload>
  implements CanActivate
{
  protected getSupportedTokenTypes(): TokenType[] {
    return [TokenType.PUBLIC];
  }
}
