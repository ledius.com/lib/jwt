/**
 *  Ledius LLC
 *  Copyright (C) 24 Dec 2021  Artem Ilinykh devsinglesly@gmail.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { DynamicModule, Global, Module } from '@nestjs/common';
import { JwtModuleOptions, JwtService } from '@nestjs/jwt';

@Global()
@Module({})
export class JsonWebTokenModule {
  public static forRoot(options: JwtModuleOptions): DynamicModule {
    const jwtServiceProvider = {
      provide: JwtService,
      useValue: new JwtService(options),
    };

    return {
      module: JsonWebTokenModule,
      global: true,
      providers: [jwtServiceProvider],
      exports: [jwtServiceProvider],
    };
  }
}
